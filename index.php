<?php
include "connection.php";
include "function.php";
if(isset($_GET['delete'])){
    deleteSiswa($_GET);
    header('Location:index.php');
}
if(isset($_POST['add'])){
    inputData($_POST);
    header('Location:index.php');
}
if(isset($_POST['tambah'])){
    edit($_POST);
    header('location:index.php');
}


$pdo=new PDO("mysql:host=localhost;dbname=latihann","root","",[PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION]);

$num = $pdo->query("SELECT halaman FROM perpustakaan");

$nm = $pdo->query("SELECT judul FROM perpustakaan");


?>

<!DOCTYPE html>
<html>
<head>
    <title>Dashboard</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <style>
        body {
            font-family: "Lato", sans-serif;
            color: white;
        }
        
        .sidenav {
            height: 100%;
            width: 0;
            position: fixed;
            z-index: 1;
            top: 0;
            left: 0;
            background-color: #111;
            overflow-x: hidden;
            transition: 0.5s;
            padding-top: 60px;
        }
        
        .sidenav a {
            padding: 8px 8px 8px 32px;
            text-decoration: none;
            font-size: 25px;
            color: #818181;
            display: block;
            transition: 0.3s;
        }
        
        .sidenav a:hover {
            color: #f1f1f1;
        }
        
        .sidenav .closebtn {
            position: absolute;
            top: 0;
            right: 25px;
            font-size: 36px;
            margin-left: 50px;
        }
        
        @media screen and (max-height: 450px) {
            .sidenav {padding-top: 15px;}
            .sidenav a {font-size: 18px;}
        }
        body {
            background-image: url(img/back.jpg);   
        }
    </style>
</head>
</head>
<body>
    
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <div id="mySidenav" class="sidenav">
                    <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
                    <a href="index.php">Dashboard</a>
                    <a href="#exampleModal" data-target="#exampleModal" data-toggle="modal">+Daftar Buku</a>
                </div> 
                <span style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; open dashboard</span>
            </div>
            <h5 style="float-right;" class="mt-1"> <?= date('d F Y') ;?></h5>
        </div>
    </div>   
    <div class="container">
        <div class="col-12 p-0">
            <div class="row text-monospace">
                <div class="col table-responsive">
                    <table class="table border text-light">
                        <thead class="thead">
                            <tr>
                                <th scope="col">Judul</th>
                                <th scope="col">Halaman</th>
                                <th scope="col">Penulis</th>
                                <th scope="col">Opsi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($data as $key) :?>
                            <tr>
                                <th><?php echo $key['judul'];?></th>
                                <td><?php echo $key['halaman'];?></td>
                                <td><?php echo $key['penulis'];?></td>
                                <td>
                                    <a class="btn btn-light" onclick="return confirm('Apakah Kamu Ingin Menghapus Daftar buku ini?')"  href="index.php?delete=&id=<?php echo $key['id']?>">Delete</a>
                                </td>
                                
                            </tr>
                            <?php endforeach ;?>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- POPUP Tambah Data -->
            <div class="col-sm-12 col-md-4 col-lg-4">
                <div class="modal fade" id="exampleModal"  data-backdrop="static" data-keyboard="false" tabindex="-1">
                    <div class="modal-dialog text-monospace" role="document">
                        <div class="modal-content">
                            <div class="modal-body">
                                <form action="index.php" method="POST">
                                    <div class="form-group">
                                        <h3 class="text-dark">Tambahkan buku baru</h3>
                                        <label>Nama buku</label>
                                        <input type="text" name="judul" class="form-control" required placeholder="judul" autocomplete="off">
                                    </div>
                                    <div class="form-group">
                                        <label>Halaman</label>
                                        <input type="number" id="halaman" name="halaman" class="form-control" required placeholder="Halaman" autocomplete="off">
                                    </div>
                                    <div class="form-group">
                                        <label>Penulis</label>
                                        <input type="text" name="penulis" class="form-control" required placeholder="Penulis" autocomplete="off">
                                    </div>
                                    <button type="submit" name="add" class="btn btn-primary">Kirim Data</button>
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>            </form>  
                                </div>
                            </div>
                        </div>
                    </div>
                </div>  
            </div>
            <div class="mt-2 col-sm-12">
                <canvas id="myChart" width="100px" height="33px"></canvas>
            </div>
        </div>    
        
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
        <script src="Chart.js"></script>
        <script>
            function openNav() {
                document.getElementById("mySidenav").style.width = "250px";
            }
            
            function closeNav() {
                document.getElementById("mySidenav").style.width = "0";
            }
            
            var ctx = document.getElementById("myChart").getContext('2d');
            
            var myChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: [<?php foreach ($nm as $key) { echo '"'.$key['judul'].'",'; } ?>],
                    datasets: [{
                        label: 'Halaman',
                        data: [<?php foreach ($num as $key) { echo '"'.$key['halaman'].'",'; } ?>],
                        backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                        ],
                        borderColor: [
                        'rgba(255,99,132,1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                        ],
                        borderWidth: 1
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero:true
                            }
                        }]
                    }
                }
            });
        </script>
    </body>
    </html> 
    